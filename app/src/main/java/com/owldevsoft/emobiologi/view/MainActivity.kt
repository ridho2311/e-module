package com.owldevsoft.emobiologi.view

import android.media.MediaPlayer
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.ActionBarDrawerToggle
import com.owldevsoft.emobiologi.R
import com.owldevsoft.emobiologi.view.fragments.*
import kotlinx.android.synthetic.main.activity_main.*
import java.io.IOException

class MainActivity : AppCompatActivity() {
    private var toggle : ActionBarDrawerToggle? = null
    private var mp : MediaPlayer? = null
    private var playing = true
    private var resume = true
    private var lenght = 0

    override fun onResume() {
        super.onResume()
        if (resume && playing){
            resume = false
            mp?.seekTo(lenght)
            mp?.start()
            lenght = 0
        }
    }

    override fun onPause() {
        super.onPause()
        if (playing){
            resume = true
            mp?.pause()
            if (mp?.currentPosition != null)
                lenght = mp?.currentPosition!!
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        mp = MediaPlayer()
        playSound()

        toggle = ActionBarDrawerToggle(this, draw, toolbar, R.string.open, R.string.close)
        toggle?.syncState()
        draw.addDrawerListener(toggle!!)
        nav_view.setNavigationItemSelectedListener {
            val id = it.itemId
            var frag : Fragment? = HomeFragment()
            when(id){
                R.id.menu_home -> {
                    frag = HomeFragment()
                }
                R.id.menu_tutorial -> {
                    frag = TutorialFragment()
                }
                R.id.menu_pendahuluan -> {
                    frag = PendahuluanFragment()
                }
                R.id.menu_ki -> {
                    frag = KiFragment()
                }
                R.id.menu_materi -> {
                    frag = MateriFragment()
                }
                R.id.menu_tf -> {
                    frag = TesFragment()
                }
                R.id.menu_glosarium -> {
                    frag = GlosariumFragment()
                }
                R.id.menu_dp -> {
                    frag = PustakaFragment()
                }
                R.id.menu_sound -> {
                    playing = if (playing){
                        mp?.stop()
                        false
                    }else{
                        playSound()
                        true
                    }
                }
                R.id.menu_profile -> {
                    frag = ProfileFragment()
                }
                R.id.menu_quit -> { finish() }
            }
            if (frag != null) {
                supportFragmentManager.beginTransaction().replace(R.id.frame, frag).commit()
            }
            draw.closeDrawers()
            return@setNavigationItemSelectedListener true
        }
        supportFragmentManager.beginTransaction().replace(R.id.frame, HomeFragment()).commit()
    }

    private fun playSound() {
        val path = "audio/backsound.mp3"
        try {
            val descriptor = baseContext.assets.openFd(path)
            val start = descriptor.startOffset
            val end = descriptor.length
            mp!!.reset()
            mp!!.setDataSource(descriptor.fileDescriptor, start, end)
            mp!!.prepare()
            mp!!.start()
            mp?.isLooping = true

        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mp?.stop()
    }
}

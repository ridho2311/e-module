package com.owldevsoft.emobiologi.view.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.owldevsoft.emobiologi.R
import com.owldevsoft.emobiologi.adapter.LockA
import com.owldevsoft.emobiologi.adapter.QuestionA
import com.owldevsoft.emobiologi.model.Question

class TesFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_tes, container, false)

        val rvQuestion = view.findViewById<RecyclerView>(R.id.rv_test)
        rvQuestion.layoutManager = LinearLayoutManager(context)
        val listQuestion = quest()
            rvQuestion.adapter = QuestionA(listQuestion, object : QuestionA.AnswerListener {
                override fun onAnswer(position: Int, ans: String) {
                    val data = listQuestion[position]
                    data.Answer = ans
                    listQuestion[position] = data
                    rvQuestion.adapter?.notifyItemChanged(position, data)
                }
            })
            rvQuestion.visibility = View.VISIBLE

        val rvLock = view.findViewById<RecyclerView>(R.id.rv_lock)
        rvLock.layoutManager = LinearLayoutManager(context)
        val listLock = lock()
        rvLock.adapter = LockA(listLock)

        val checkResult = view.findViewById<LinearLayout>(R.id.l_calculation)
        val txResult = view.findViewById<TextView>(R.id.tx_result)
        val result = view.findViewById<LinearLayout>(R.id.l_result)
        val imgResult = view.findViewById<ImageView>(R.id.img_nilai)

        checkResult.setOnClickListener {
            imgResult.setImageResource(R.drawable.ic_check_black_24dp)
            result.visibility = View.VISIBLE
            var res = 0
            for ((idx, value) in listQuestion.withIndex()) {
                if (value.Answer == listLock[idx])
                    res += 1
            }
            val resultFinal = res * 100 / listQuestion.size
            var msg = ""
            if (resultFinal <= 30)
                msg = "(Belajar lebih giat lagi)"
            else if (resultFinal <= 60)
                msg = "(Sangat kurang)"
            else if (resultFinal <= 80)
                msg = "(Baik)"
            else if (resultFinal >= 81)
                msg = "(Memuaskan)"
            txResult.text = "$resultFinal $msg"
        }
        return view
    }

    private fun quest(): ArrayList<Question> {
        val q = ArrayList<Question>()
        val q1 = Question(
            "1. Serebrum dibagi menjadi empat lobus utama. Masing- masing memiliki fungsi dan peran yang berbeda. Bagian mana yang meregulasi kontrol motorik halus, seperti gerakan yang terlibat dalam berbicara dan berperan sebagai pengorganisasi stimuli sensoris yang masuk?",
            "A. Lobus frontal",
            "B. Lobus oksipitalis",
            "C. Lobus temporal",
            "D. Lobus parietal", null, ""
        )
        q.add(q1)
        val q2 = Question(
            "2. Sistem saraf otonom terbagi atas sistem saraf simpatik dan parasimpatik. Kerja kedua saraf ini saling berlawanan sehingga mengakibatkan keadaan yang normal. Sistem saraf manakah yang menstimulasi sekresi dan gerakan peristaltik pada lambung?",
            "A. Saraf simpatik",
            "B. Saraf parasimpatik",
            "C. Saraf somatik",
            "D. Saraf peristaltik", null, ""
        )
        q.add(q2)
        val q3 = Question(
            "3. Perhatikan skema proses gerak refleks. Berdasarkan skema tersebut yang ditandai O, P, Q dan R secara urut adalah...",
            "A. Efektor, sumsum tulang belakang, saraf sensorik, reseptor",
            "B. Reseptor, sumsum tulang belakang, saraf sensorik, efektor",
            "C. Reseptor, saraf sensorik, sumsum tulang belakang, efektor",
            "D. reseptor, saraf motorik, sumsum tulang belakang, efektor", R.drawable.picture1, ""
        )
        q.add(q3)
        val q4 = Question(
            "4. Cermati pernyataan- pernyataan berikut!\n" +
                    "(1) Adanya perbedaan potensial listrik antara bagian luar dan bagian dalam sel\n" +
                    "(2) Terdiri atas neuron-neuron individual yang tidak saling berhubungan\n" +
                    "(3)Pada waktu sel saraf berisitirahat, kutub positif berada dibagian luar dan kutub negatif berada dibagian dalam sel saraf\n" +
                    "(4)Dekatnya jarak yang harus dilalui dan cepatnya difusi, menyebabkan cepatnya transmisi yang terjadi pada sinapsis\n" +
                    "Dari pernyataan- pernyataan tersebut, yang merupakan ciri penghantaran impuls melalui sinapsis adalah...",
            "A. (3) dan (4)",
            "B. (2) dan (1)",
            "C. (1) dan (3)",
            "D. (2) dan (4)", null, ""
        )
        q.add(q4)
        val q5 = Question(
            "5. Seseorang yang memegang benda panas, secara reflek akan menjauhkan tangannya dari benda tersebut. Hal ini terjadi karna adanya rangsangan yang diterima oleh hipotalamus yang dilanjutkan ke lapisan kulit yang menyebabkan tangan menjauh. Bagaimana mekanisme penerimaan impuls oleh sel saraf?",
            "A. Impuls- indra- neuron sensorik- saraf pusat- neuron motorik-efektor-gerak",
            "B. Impuls-neuron sensorik- indra- saraf pusat- neuron motorik- efektor- gerak",
            "C. Impuls- indra- gerak- neuron sensorik- saraf pusat- neuron motorik- efektor",
            "D. Impuls- neuron motorik- gerak- neuron sensorik- saraf pusat- indra- efektor",
            null,
            ""
        )
        q.add(q5)
        return q
    }

    private fun lock(): ArrayList<String> {
        val list = ArrayList<String>()
        list.add("A")
        list.add("B")
        list.add("C")
        list.add("D")
        list.add("A")
        return list
    }
}

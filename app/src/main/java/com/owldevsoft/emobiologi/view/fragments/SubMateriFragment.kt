package com.owldevsoft.emobiologi.view.fragments


import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.owldevsoft.emobiologi.R


class SubMateriFragment : Fragment() {
    private val bundle = Bundle()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val bundle = arguments
        val materi = bundle?.getString("materi")
        return when (materi) {
            "1" -> {
                val view = inflater.inflate(R.layout.fragment_sub_materi1, container, false)
                initMateri1(view)
                return view
            }
            "2" -> {
                val view = inflater.inflate(R.layout.fragment_sub_materi2, container, false)
                initMateri2(view)
                return view
            }
            "3" -> {
                val view = inflater.inflate(R.layout.fragment_sub_materi3, container, false)
                initMateri3(view)
                return view
            }
            "prediksiM1" -> inflater.inflate(R.layout.prediksi_materi1, container, false)
            "explorasiM1" -> inflater.inflate(R.layout.bereksplorasi_materi1, container, false)
            "rangkumanM1" -> inflater.inflate(R.layout.rangkuman_materi1, container, false)
            "eksperimentM1" -> inflater.inflate(R.layout.eksperiment_materi1, container, false)
            "explorasiM2" -> inflater.inflate(R.layout.bereksplorasi_materi2, container, false)
            else -> {
                return null
            }
        }
    }

    private fun initMateri3(view: View?) {
        view?.findViewById<LinearLayout>(R.id.l_materi_plus)?.setOnClickListener {
            val url = "https://blog.ruangguru.com/biologi-kelas-11-kelainan-sistem-saraf-dan-sistem-endokrin"
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            startActivity(i)
        }
    }

    private fun initMateri2(view: View?) {
        view?.findViewById<LinearLayout>(R.id.l_explorasi)?.setOnClickListener {
            bundle.putString("materi", "explorasiM2")
            val fragment = SubMateriFragment()
            fragment.arguments = bundle
            fragmentManager?.beginTransaction()?.add(R.id.frame, fragment)?.addToBackStack("back")
                ?.commit()
        }
        view?.findViewById<LinearLayout>(R.id.l_vidio)?.setOnClickListener {
            val appIntent = Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:kqDHNGYoroE"))
            val webIntent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("https://www.youtube.com/watch?v=kqDHNGYoroE")
            )
            try {
                context?.startActivity(appIntent)
            } catch (ex: ActivityNotFoundException) {
                context?.startActivity(webIntent)
            }

        }
        view?.findViewById<LinearLayout>(R.id.l_vidio1)?.setOnClickListener {
            val appIntent = Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:T7MhbBwKk8k"))
            val webIntent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("https://www.youtube.com/watch?v=T7MhbBwKk8k")
            )
            try {
                context?.startActivity(appIntent)
            } catch (ex: ActivityNotFoundException) {
                context?.startActivity(webIntent)
            }

        }
        view?.findViewById<LinearLayout>(R.id.l_vidio2)?.setOnClickListener {
            val appIntent = Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:jFvRKR9lRIc"))
            val webIntent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("https://www.youtube.com/watch?v=jFvRKR9lRIc")
            )
            try {
                context?.startActivity(appIntent)
            } catch (ex: ActivityNotFoundException) {
                context?.startActivity(webIntent)
            }

        }
        view?.findViewById<LinearLayout>(R.id.l_vidio3)?.setOnClickListener {
            val appIntent = Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:aM9JAp88c0U"))
            val webIntent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("https://www.youtube.com/watch?v=aM9JAp88c0U")
            )
            try {
                context?.startActivity(appIntent)
            } catch (ex: ActivityNotFoundException) {
                context?.startActivity(webIntent)
            }

        }
        view?.findViewById<LinearLayout>(R.id.l_vidio_plus)?.setOnClickListener {
            val appIntent = Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:aM9JAp88c0U"))
            val webIntent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("https://www.youtube.com/watch?v=QiVZG6HKT00")
            )
            try {
                context?.startActivity(appIntent)
            } catch (ex: ActivityNotFoundException) {
                context?.startActivity(webIntent)
            }
        }
        view?.findViewById<LinearLayout>(R.id.l_materi_plus)?.setOnClickListener {
            val url = "https://blog.ruangguru.com/biologi-kelas-11-mengenal-sistem-saraf-manusia"
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            startActivity(i)
        }
    }

    private fun initMateri1(view: View?) {
        view?.findViewById<LinearLayout>(R.id.l_vidio)?.visibility = View.GONE
        view?.findViewById<LinearLayout>(R.id.l_prediksi)?.setOnClickListener {
            bundle.putString("materi", "prediksiM1")
            val fragment = SubMateriFragment()
            fragment.arguments = bundle
            fragmentManager?.beginTransaction()?.add(R.id.frame, fragment)?.addToBackStack("back")
                ?.commit()
        }
        view?.findViewById<LinearLayout>(R.id.l_explorasi)?.setOnClickListener {
            bundle.putString("materi", "explorasiM1")
            val fragment = SubMateriFragment()
            fragment.arguments = bundle
            fragmentManager?.beginTransaction()?.add(R.id.frame, fragment)?.addToBackStack("back")
                ?.commit()
        }
        view?.findViewById<LinearLayout>(R.id.l_rangkuman)?.setOnClickListener {
            bundle.putString("materi", "rangkumanM1")
            val fragment = SubMateriFragment()
            fragment.arguments = bundle
            fragmentManager?.beginTransaction()?.add(R.id.frame, fragment)?.addToBackStack("back")
                ?.commit()
        }
        view?.findViewById<LinearLayout>(R.id.l_experiment)?.setOnClickListener {
            bundle.putString("materi", "eksperimentM1")
            val fragment = SubMateriFragment()
            fragment.arguments = bundle
            fragmentManager?.beginTransaction()?.add(R.id.frame, fragment)?.addToBackStack("back")
                ?.commit()
        }
        view?.findViewById<LinearLayout>(R.id.l_vidio_plus)?.setOnClickListener {
            val appIntent = Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:aM9JAp88c0U"))
            val webIntent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("https://www.youtube.com/watch?v=QiVZG6HKT00")
            )
            try {
                context?.startActivity(appIntent)
            } catch (ex: ActivityNotFoundException) {
                context?.startActivity(webIntent)
            }
        }
        view?.findViewById<LinearLayout>(R.id.l_materi_plus)?.setOnClickListener {
            val url = "https://blog.ruangguru.com/biologi-kelas-11-mengenal-sistem-saraf-manusia"
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            startActivity(i)
        }
    }


}

package com.owldevsoft.emobiologi.view

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.owldevsoft.emobiologi.R
import kotlinx.android.synthetic.main.activity_start.*

class StartActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)
        btn_play.setOnClickListener {
            startActivity(Intent(baseContext, MainActivity::class.java))
        }
    }
}

package com.owldevsoft.emobiologi.view.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.owldevsoft.emobiologi.R

class MateriFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_materi, container, false)
        val bundle = Bundle()
        view.findViewById<LinearLayout>(R.id.btn_materi1).setOnClickListener {
            bundle.putString("materi", "1")
            val fragment = SubMateriFragment()
            fragment.arguments = bundle
            fragmentManager?.beginTransaction()?.add(R.id.frame, fragment)?.addToBackStack("back")?.commit()
        }
        view.findViewById<LinearLayout>(R.id.btn_materi2).setOnClickListener {
            bundle.putString("materi", "2")
            val fragment = SubMateriFragment()
            fragment.arguments = bundle
            fragmentManager?.beginTransaction()?.add(R.id.frame, fragment)?.addToBackStack("back")?.commit()
        }
        view.findViewById<LinearLayout>(R.id.btn_materi3).setOnClickListener {
            bundle.putString("materi", "3")
            val fragment = SubMateriFragment()
            fragment.arguments = bundle
            fragmentManager?.beginTransaction()?.add(R.id.frame, fragment)?.addToBackStack("back")?.commit()
        }
        return view
    }
}

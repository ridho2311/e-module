package com.owldevsoft.emobiologi.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.owldevsoft.emobiologi.R
import kotlinx.android.synthetic.main.item_lock.view.*

class LockA(val list : ArrayList<String>) : RecyclerView.Adapter<LockA.VhLock>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): VhLock {
        val view = LayoutInflater.from(p0.context).inflate(R.layout.item_lock, p0, false)
        return VhLock(view)
    }

    override fun getItemCount(): Int = list.size
    override fun onBindViewHolder(p0: VhLock, p1: Int) {
        p0.onBind(p1, list[p1])
    }

    class VhLock(itemView: View) : RecyclerView.ViewHolder(itemView){
        val lock = itemView.findViewById<TextView>(R.id.tx_lock)
        fun onBind(position : Int,data : String){
            lock.text = "${position+1}. $data"
        }
    }
}
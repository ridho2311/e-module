package com.owldevsoft.emobiologi.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.owldevsoft.emobiologi.R
import com.owldevsoft.emobiologi.model.Question

class QuestionA(private val list: ArrayList<Question>, private val answerListener: AnswerListener) : RecyclerView.Adapter<QuestionA.VhQuestion>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): VhQuestion {
        val view = LayoutInflater.from(p0.context).inflate(R.layout.item_question, p0, false)
        return VhQuestion(view)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(p0: VhQuestion, p1: Int) {
        p0.onBind(p1, list[p1], answerListener)
    }

    class VhQuestion(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val img = itemView.findViewById<ImageView>(R.id.img_question)
        val qna = itemView.findViewById<TextView>(R.id.txQuestion)
        val qA = itemView.findViewById<TextView>(R.id.txAnsA)
        val qB = itemView.findViewById<TextView>(R.id.txAnsB)
        val qC = itemView.findViewById<TextView>(R.id.txAnsC)
        val qD = itemView.findViewById<TextView>(R.id.txAnsD)
        fun onBind(position: Int, data: Question, answerListener: AnswerListener) {
            qna.text = data.question
            qA.text = data.qA
            qB.text = data.qB
            qC.text = data.qC
            qD.text = data.qD
            if(data.img != null){
                img.visibility = View.VISIBLE
                img.setImageResource(data.img)
            }
            when (data.Answer) {
                "A" -> ansA()
                "B" -> ansB()
                "C" -> ansC()
                "D" -> ansD()
            }
            qA.setOnClickListener {
                answerListener.onAnswer(position, "A")
                ansA()
            }
            qB.setOnClickListener {
                answerListener.onAnswer(position, "B")
                ansB()
            }
            qC.setOnClickListener {
                answerListener.onAnswer(position, "C")
                ansC()
            }
            qD.setOnClickListener {
                answerListener.onAnswer(position, "D")
                ansD()
            }
        }

        private fun ansA() {
            qA.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_check_black_24dp, 0)
            qB.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
            qC.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
            qD.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
        }

        private fun ansB() {
            qB.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_check_black_24dp, 0)
            qA.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
            qC.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
            qD.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
        }

        private fun ansC() {
            qC.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_check_black_24dp, 0)
            qB.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
            qA.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
            qD.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
        }

        private fun ansD() {
            qD.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_check_black_24dp, 0)
            qB.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
            qC.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
            qA.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
        }
    }

    interface AnswerListener {
        fun onAnswer(position: Int, ans: String)
    }
}